000001  /* REXX */
000002  /* Extracts the job information for my main process */
000003  /* Pull in the arg */
000004  Arg outmem
000005  /* Test if it is set, else set default for testing */
000006   if outmem = '' then
000007     outmem = 'tjes2jcl'
000008         /* Allocate the output dataset */
000009  "allocate dataset(testout("outmem")) fi(theLine) shr reuse"
000010  "execio 0 diskw theLine (stem theLine. open)"
000011  /* Allocate input dataset */
000012  "allocate dataset(testout(tmp)) fi(tmp) shr reuse"
000013  "execio * diskr tmp (stem tmp. finis)"
000014  /* loop through the lines from the output from the JCL */
000015   do i=1 to tmp.0
000016     currentLine = substr(tmp.i,44,77)
000017     parse var currentLine w1 w2 w3 w4
000018     /* Is it me? */
000019     if w3 == 'Z31321' then
000020       theLine.i = currentLine
000021       say tmp.i
000022   end
000023         /* close the output file */
000024  "execio * diskw theLine  (stem theLine.  finis)"
000025         /* release datasets */
000026  "free fi(tmp)"
000027  "free fi(theLine)"
000028  exit
